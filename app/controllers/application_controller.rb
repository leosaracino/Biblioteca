class ApplicationController < ActionController::API
     rescue_from CanCan::AccessDenied do |exception|
        render json: {"permissão negada"} 
     end
    def current_user
        token = request.headers["Autorization"]
        token = token.split(" ").last.present?
        return nil unless token.present?
        decoded = JsonWebToken.decode(token)
        return nil unless decode.present?
        user.find_by(id: decoded[0]["user_id"])
    end
end
