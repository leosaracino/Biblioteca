class AuthenticationController < ApplicationController
  load_and_authorize_resource only: :sign_up 
  def login
    user = User.find_by(email: params[:user][:email])
    user = user&.authenticate(params[:user][:password])
    if user
      token = JsonWebToken.encode(user_id: user.id)
      render json: {token: token}
    else
      render json: {message:"Não foi possivel fazer login"}, status: 401
    end
  end

  def sign_up
    if current_user.role == "clerk" and user_params[:role] == "client"
      
      @user = User.new(user_params)

      if @user.save
        render json: @user, status: :created, location: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    elsif current_user.role == "admin"

      @user = User.new(user_params)
      if @user.save
        render json: @user, status: :created, location: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  end
  private
    def user_params
    params.require(:user).permit(:password, :password_confirmation, :email, :name, :birthdate, :cpf, :telephone, :role)
    end
end
