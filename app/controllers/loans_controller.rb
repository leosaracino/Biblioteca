class LoansController < ApplicationController
  before_action :set_loan, only: [:show, :update, :destroy, :close_loan]
  load_and_authorize_resource
  # GET /loans
  def index
    @loans = Loan.all

    render json: @loans
  end

  # GET /loans/1
  def show
    render json: @loan
  end

  # POST /loans
  def create
    @loan = Loan.new(loan_params)
    user = User.find(@loan.user_id)
    if (user.loans.last.status == "Ativo")
      render json: {message: "Ja possui uma Loan ativa"}
    else
      @loan.fine = 0  
      @loan.status = "Ativo"
      @loan.delay_days = 0
      if @loan.save
        render json: @loan, status: :created, location: @loan
      else
        render json: @loan.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /loans/1
  def update
    if @loan.update(loan_params)
      render json: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  # DELETE /loans/1
  def destroy
    @loan.destroy
  end

  # Post /close_loan/1
  def close_loan()
    atraso(@loan)
    @loan.fine = 2 * @loan.delay_days.to_f
    @loan.status = "Finalizado"
    if @loan.save
      render json: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan
      @loan = Loan.find(params[:id])
    end
    # Only allow a list of trusted parameters through.
    def loan_params
      params.require(:loan).permit(:book_id, :user_id, :expiry_date)
    end
end
