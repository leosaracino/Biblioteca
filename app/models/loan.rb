class Loan < ApplicationRecord
  belongs_to :book
  belongs_to :user

  def self.atraso()
    if(Date.today > @loan.expiry_date)
      @loan.delay_days = Date.today - @loan.expiry_date
    end
  end
end
