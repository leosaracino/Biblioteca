class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.date :birthdate
      t.string :cpf
      t.string :telephone
      t.string :role

      t.timestamps
    end
  end
end
