class Change < ActiveRecord::Migration[6.1]
  def change
    change_table :loans do |t|
      t.change :expiry_date , :date
      t.change :delay_days , :date
      t.change :fine , :float
    end
  end
end
